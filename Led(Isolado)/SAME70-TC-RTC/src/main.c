#include "asf.h"

/*Variaveis Globais*/
#define NUM_LEDS 4
#define NUM_LINHA 1
#define NUM_COLUNA 4

//Symbolos ns
#define TOH 0.4  
#define TOL 0.9

#define T1H 0.9
#define T1L 0.6

#define RES 7 

#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   31
#define LED_PIN_MASK   (1 << LED_PIN)

//BRG
#define BLUE   0x00FF0000
#define RED    0x0000FF00
#define GREEN  0x000000FF
#define ERASE  0x00000000

uint32_t led[NUM_LINHA][NUM_COLUNA];


inline void delay_ns(int n);
void signal_0();
void signal_1();
void show();
void send_bit(uint32_t pixel);



void LED_init(int estado){	
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
};

/*
OPTIMIZE_HIGH
RAMFUNC
void delay_hZero(){
		__asm (
		"MOV    R0, #0x10 \n"
		"loophZero: DMB	\n"
		"SUBS R0, R0, #1  \n"
		"BNE.N loophZero         "
		);
}

OPTIMIZE_HIGH
RAMFUNC
 void delay_ns(int n){	
	//int c = n ;
	//portable_delay_cycles(c);
	LED_PIO->PIO_CODR = LED_PIN_MASK;
	__asm (
	"  NOP"
	);
	//portable_delay_cycles_nano(1);
	LED_PIO->PIO_SODR = LED_PIN_MASK;
		
	__asm (
		"MOV    R0, #0x0A \n"  
		"loop: DMB	\n"
		"SUBS R0, R0, #1  \n"
		"BNE.N loop         "
	);
	
	LED_PIO->PIO_CODR = LED_PIN_MASK;
	__asm (
	"  NOP"
	);
}

// Delay loop is put to SRAM so that FWS will not affect delay time
OPTIMIZE_HIGH
RAMFUNC
inline void portable_delay_cycles_nano(unsigned long n)
{
	UNUSED(n);

	__asm (
	"  NOP"
	);
}

*/
void signal_1(){
	int i = 0;
	for(i=0; i<52; i++) // 0.7us
	LED_PIO->PIO_SODR = LED_PIN_MASK;

	for(i=0; i<44; i++) // 0.6us
	LED_PIO->PIO_CODR = LED_PIN_MASK;

}

void signal_0(){
	int i = 0;
	for(i=0; i<26; i++)	// 0.34us
	LED_PIO->PIO_SODR = LED_PIN_MASK;
	
	for(i=0; i<59; i++) // 0.8us
	LED_PIO->PIO_CODR = LED_PIN_MASK;
}

void send_bit(uint32_t pixel){
	for(int i = 0; i < 24; i++){
		if(pixel >> i & 0x00000001){
			signal_1();
		}
		else{
			signal_0();
		}
	}
}

void show(){
	LED_PIO->PIO_CODR = LED_PIN_MASK;
	delay_us(900);
}

void reset(){
	for (int i = 0; i < NUM_LEDS; i++){
		send_bit(ERASE);
	}
	show();
}

void atualiza(){
	reset();
	
	for(int i = 0; i < NUM_LINHA; i++){
		for(int j = 0; j < NUM_COLUNA; j++){
			if(led[i][j] == NULL){
				send_bit(ERASE);
			}
			else{
				send_bit(led[i][j]);
			}
		}
	}
	show();
}

void ciclagem(){
	uint32_t ledCopia[NUM_LINHA][NUM_COLUNA];
	for(int i = 0; i < NUM_LINHA; i++){
		for(int j = 0; j < NUM_COLUNA; j++){
			ledCopia[(i + 1) % NUM_LEDS][j] = led[i][j];
		}
	}
	for(int i = 0; i < NUM_LINHA; i++){
		for(int j = 0; j < NUM_COLUNA; j++){
				led[i][j] = ledCopia[i][j];
		}
	}
}


/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	LED_init(0);
	LED_PIO->PIO_CODR = LED_PIN_MASK;
	
	int Z
	
	if(score_Player1 == 1){
		led[2][2] = RED;
		led[2][3] = RED;
		led[2][4] = RED;
		led[2][5] = RED;
		led[2][6] = RED;
		
		led[3][4] = RED;
		led[4][4] = RED;
		led[5][4] = RED;
		led[6][4] = RED;
		
		led[5][3] = RED;
	}
	
	else if(score_Player1 == 0){
		led[2][2] = RED;
		led[2][3] = RED;
		led[2][4] = RED;
		led[2][5] = RED;
		led[2][6] = RED;
		
		led[3][4] = RED;
		led[4][4] = RED;
		led[5][4] = RED;
		led[6][4] = RED;
		
		led[5][3] = RED;
	}
	
	if(score_Player2 == 0){
		led[2][12] = RED;
		led[2][13] = RED;
		
		
		led[3][11] = RED;
		led[4][11] = RED;
		led[5][11] = RED;
		
		
		led[6][12] = RED;
		led[6][13] = RED;
		
		led[3][14] = RED;
		led[4][14] = RED;
		led[5][14] = RED;
	}
	
	while (1) {
		//ciclagem();
		atualiza();
		delay_ms(39);
	}
}
