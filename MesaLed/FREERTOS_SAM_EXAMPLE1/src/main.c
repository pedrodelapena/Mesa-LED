#include <asf.h>
#include <gamePong.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)

/* VAR globais                                                          */
/************************************************************************/
#define NUM_LEDS 286

//Buffer
uint8_t bufferRX[100];
uint8_t bufferTX[100];

uint8_t bufferRX2[100];
uint8_t bufferTX2[100];

int wait_Animation = 0;

//Data
uint32_t led[NUM_LINHA][NUM_COLUNA];

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore1;
SemaphoreHandle_t xSemaphore2;

//LED
#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   31
#define LED_PIN_MASK   (1 << LED_PIN)

/* Funcoes                                                              */
/************************************************************************/
static void USART0_init(void);

void signal_0();
void signal_1();
void show();
void atualiza();
void resetAll();
int modulo(int valor);
void send_bit(uint32_t pixel);
uint32_t usart_getString(Usart*  usart, uint8_t *pstring);
uint32_t usart_putString(Usart* usart, uint8_t *pstring);
	
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/* Handlers                                                             */
/************************************************************************/
void USART0_Handler(void){
	uint32_t ret = usart_get_status(USART0);

	//BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	char c;

	// Verifica por qual motivo entrou na interrup�cao
	//  - Dadodispon�vel para leitura
	if(ret & US_IER_RXRDY){
		
		usart_getString(USART0, bufferRX);
		BaseType_t xHigherPriorityTaskWoken = pdTRUE;
		xSemaphoreGiveFromISR(xSemaphore1, &xHigherPriorityTaskWoken);
	}
}

void USART2_Handler(void){
	uint32_t ret = usart_get_status(USART2);

	//BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	char c;

	// Verifica por qual motivo entrou na interrup?cao
	//  - Dadodispon?vel para leitura
	if(ret & US_IER_RXRDY){
		usart_getString(USART2, bufferRX2);
		BaseType_t xHigherPriorityTaskWoken = pdTRUE;
		xSemaphoreGiveFromISR(xSemaphore2, &xHigherPriorityTaskWoken);
	}
}

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}



/**
 *  Envia para o UART uma string
 */
uint32_t usart_putString(Usart* usart, uint8_t *pstring){
  uint32_t i = 0 ;

  while(*(pstring + i)){
    usart_serial_putchar(usart, *(pstring+i++));
    while(!uart_is_tx_empty(usart)){};
  }    
     
  return(i);
}

/*
 * Busca no UART uma string
 */
uint32_t usart_getString(Usart*  usart, uint8_t *pstring){
  uint32_t i = 0 ;
  
  usart_serial_getchar(usart, (pstring+i));
  while(*(pstring+i) != '\n'){
	printf("%c",*(pstring+i));
	usart_serial_getchar(usart, (pstring+(++i)));
  }
  *(pstring+i)= 0x00;
  return(i);
}


void LED_init(int estado){
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
}


static void USART0_init(void){
	
	/* Configura USART0 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB0); //TxModulo - Rx Atmel
	pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB1); //Rx Modulo - Tx Atmel
	
	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate     = 9600,
		.char_length  = US_MR_CHRL_8_BIT,
		.parity_type  = US_MR_PAR_NO,
		.stop_bits    = US_MR_NBSTOP_1_BIT,
		.channel_mode = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART0 */
	sysclk_enable_peripheral_clock(ID_USART0);
	
	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART0, &usart_settings, sysclk_get_peripheral_hz());
	
	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART0);
	usart_enable_rx(USART0);
	
	/* ativando interrupcao */
	usart_enable_interrupt(USART0, US_IER_RXRDY);
	NVIC_SetPriority(ID_USART0, 4);
	NVIC_EnableIRQ(ID_USART0);
}



static void USART2_init(void){
	
	/* Configura USART0 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOD);
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD15); //RX - TXModulo
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD16); //TX - RXModulo
	
	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate     = 9600,
		.char_length  = US_MR_CHRL_8_BIT,
		.parity_type  = US_MR_PAR_NO,
		.stop_bits    = US_MR_NBSTOP_1_BIT,
		.channel_mode = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART2 */
	sysclk_enable_peripheral_clock(ID_USART2);
	
	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART2, &usart_settings, sysclk_get_peripheral_hz());
	
	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART2);
	usart_enable_rx(USART2);
	
	/* ativando interrupcao */
	usart_enable_interrupt(USART2, US_IER_RXRDY);
	NVIC_SetPriority(ID_USART2, 4);
	NVIC_EnableIRQ(ID_USART2);
}



/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */
static void task_Player1(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore1 = xSemaphoreCreateBinary();

	if (xSemaphore1 == NULL)
		printf("falha em criar o semaforo \n");
	
	const TickType_t xDelay = 100 / portTICK_PERIOD_MS;
	for (;;) {
			
		if ( ( xSemaphoreTake(xSemaphore1, ( TickType_t ) 0) == pdTRUE ) && (!wait_Animation)  ){
			if(bufferRX[0] == '1'){
				move_Player(1, 1, led);
				atualiza_Player(1, led);		
			}
			else if(bufferRX[0] == '2'){
				
			}
			else if(bufferRX[0] == '3'){
				move_Player(1, 3, led);
				atualiza_Player(1, led);
			}
			else if(bufferRX[0] == '4'){
			}
		
			else if(bufferRX[0] == '9'){
				resetAll();
			}
			
		
			atualiza();
		}
		vTaskDelay(xDelay);
		
	}
}


static void task_Player2(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore2 = xSemaphoreCreateBinary();

	if (xSemaphore2 == NULL)
	printf("falha em criar o semaforo \n");
	
	const TickType_t xDelay = 100 / portTICK_PERIOD_MS;
	for (;;) {
		
		if ( ( xSemaphoreTake(xSemaphore2, ( TickType_t ) 0) == pdTRUE ) && (!wait_Animation)  ){
			if(bufferRX2[0] == '1'){
				move_Player(2, 1, led);
				atualiza_Player(2, led);
			}
			else if(bufferRX2[0] == '2'){
				
			}
			else if(bufferRX2[0] == '3'){
				move_Player(2, 3, led);
				atualiza_Player(2, led);
			}
			else if(bufferRX2[0] == '4'){
			}
			
			else if(bufferRX[0] == '9'){
				resetAll();
			}
			
			atualiza();
		}
		vTaskDelay(xDelay);
	}
}




static void task_game(void *pvParameters)
{
	
	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;

	for (;;) {
		wait_Animation = move_Ball(led);
					
		if (wait_Animation){	
			resetAll();
			atualiza();
			
			for (int i = 0; i < 10; i++){
				score_animation(led);
				atualiza();
			}
			
			resetAll();
			reset_Game(led);
			wait_Animation = 0;
		}
		
		atualiza();
		vTaskDelay(xDelay);
	}
}






static void configure_console(void)
{

	/* Configura USART1 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
	
	const usart_serial_options_t uart_serial_options = {
		.baudrate   = CONF_UART_BAUDRATE,
		.charlength = CONF_UART_CHAR_LENGTH,
		.paritytype = CONF_UART_PARITY,
		.stopbits   = CONF_UART_STOP_BITS,
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


void signal_1(){
	int i = 0;
	for(i = 0; i < 52; i++) // 0.7us
	LED_PIO->PIO_SODR = LED_PIN_MASK;

	for(i = 0; i < 44; i++) // 0.6us
	LED_PIO->PIO_CODR = LED_PIN_MASK;

}

void signal_0(){
	int i = 0;
	for(i = 0; i < 26; i++)	// 0.34us
	LED_PIO->PIO_SODR = LED_PIN_MASK;
	
	for(i = 0; i < 59; i++) // 0.8us
	LED_PIO->PIO_CODR = LED_PIN_MASK;
}

void send_bit(uint32_t pixel){
	for(int i = 0; i < 24; i++){
		if(pixel >> i & 0x00000001){
			signal_1();
		}
		else{
			signal_0();
		}
	}
}

void resetAll(){
	for(int i = 0; i < NUM_LINHA; i++){
		for(int j = 0; j < NUM_COLUNA; j++){
			led[i][j] = ERASE;
		}
	}
}

void show(){
	LED_PIO->PIO_CODR = LED_PIN_MASK;
	delay_us(1000);
}


int modulo(int valor){
	if (valor < 0) {
		return valor * -1;
	}
	return valor;
} 

void atualiza(){
	for(int i = 0; i < NUM_LINHA; i++){
		for(int j = 0; j < NUM_COLUNA; j++){

			if(i % 2 == 0){
				send_bit(led[i][j]);
				}
				
			else{
				send_bit(led[i][modulo(j - (NUM_COLUNA - 1))]);
			}
			
		}
	}
	show();
}


/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();
	
	USART0_init();
	USART2_init();
	LED_init(0);
	
	//Chacar caso de errado
	LED_PIO->PIO_CODR = LED_PIN_MASK;
	
	/* Initialize the console uart */
	configure_console();
	start_Pong(led);
	atualiza();

	/* Create task to make led blink */
	if (xTaskCreate(task_Player1, "P1", TASK_LED_STACK_SIZE, NULL,
			3, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_Player2, "P2", TASK_LED_STACK_SIZE, NULL,
	3, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	
	if (xTaskCreate(task_game, "Game", TASK_LED_STACK_SIZE, NULL,
		0, NULL) != pdPASS) {
		printf("Failed to create test game task\r\n");
	}
	
	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
