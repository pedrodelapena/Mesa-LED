# Projeto Final - Mesa LED
<p>Por Leonardo Medeiros e Pedro de la Peña</p>

## Introdução
Um embarcado com a funcionalidade de ser um tabuleiro para jogos que não precisam de tanta resolução, como por exemplo PONG, Snake e Tetris. 
Utilizando um ou dois celulares com conexão Bluetooth, é possível se diverir com seus amigos jogando uma partida casual de um destes jogos clássicos.

## Funcionalidade 
O projeto consiste em uma matriz 14x20, totalizando 286 LEDs RGB endereçáveis (WS2812) e um microcontrolador do tipo Atmel SAM E70 (ARM Cortex-M7).
Com o auxílio de dois módulos Bluetooth HC05 conectados à placa por UART e um aplicativo próprio, é possível enviar comandos para o jogo. Devido à grande quantidade de LEDs, é necessária alimentação externa.
Para isto, foi utilizada uma fonte de 12V e 20A. Apesar da alta amperagem da fonte, a matriz não consome mais que 1A e 5V.

## Implementação
Foi preciso implementar por completo uma biblioteca para o controle dos LEDs. Para isto, foi necessário entender como o envio de dados era realizado pela fita. 
Os dados são transmitidos sequencialmente, em forma de cascata. Para acionar um LED, portanto, é necessário enviar 3 bytes contendo as informações de cor, seguindo o modelo GBR.

<img src="./imgs/cascata.png" width="350"/>

Além disso, a transmissão dos bytes é realizada com base no tempo em high e low. Desta forma, a transmissão de "1" e "0" é dada por:

<img src="./imgs/time.png" width="350"/>

Sendo que "RET code" é o tempo necessário para que um LED seja aceso com as informações passadas à ele.

## Viabilidade econômica
O preço total do projeto ficou por volta de 394 reais, sendo que R$ 160 foram gastos na fita WS2812, R$ 35 foram gastos na fonte 12V 20A, R$ 35 foram gastos com madeira MDF e acrílico fosco, 
R$ 7 foram gastos em cada um dos módulos HC05 e a placa Atmel SAM E70 custa por volta de R$ 150.
É possível reduzir os custos utilizando outro tipo de material para a construção do tabuleiro e também é possível substituir a fonte por uma que forneça 1.5A e 5V como foi dito na seção de funcionalidades.

## Diagrama de blocos
<img src="./imgs/diagrama.png" width="750"/>
<p>A partir deste diagrama, é possivel visualizar que ambos módulos Bluetooth estão conectados em UARTs diferentes, sendo que cada uma delas possui um handler e uma task próprias, referentes a cada um dos jogadores, além de uma task para o jogo.
Por meio de interrupções, é possível garantir que ambos jogadores e o ambiente do jogo se movimentem de maneira simultânea, sem a necessidade de longos periodos de espera para a realização de um movimento, garantindo fluidez ao jogo.
Além disso, há uma task Idle para momentos em que não há ações dos jogadores, entrando em sleep mode e garantindo economia energética.

## Video de demonstração
https://www.youtube.com/watch?v=S8mxI0TublU
